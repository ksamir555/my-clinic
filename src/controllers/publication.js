const { auth, publication } = require('../services');

const { requireAuthentication } = auth;
const { createPublication, findPublications } = publication;

function getPublications() {
  return findPublications();
}

function postPublication(content) {
  // requireAuthentication(user);
  return createPublication(content);
}

module.exports = {
  getPublications,
  postPublication,
};
