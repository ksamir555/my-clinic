FROM node:8.11.2
RUN apt-get update

# RUN npm install -g npm@5.6.0

# RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927 \
#   && echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.2 main" | tee /etc/apt/sources.list.d/mongodb-org-3.2.list \
#   && apt-get update \
#   && apt-get install -y mongodb-org --no-install-recommends \
#   && apt-get clean \
#   && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
# RUN mongod start


ADD ./ /app

WORKDIR /app

RUN npm install

RUN npm i -g nodemon

EXPOSE 3030

CMD npm run start-dev

# CMD while true; do sleep 1000; done