module.exports = {
  // Databases.
  database: {
    // MongoDB.
    data: {
      host: '172.16.202.12',
      port: 27019,
      db: 'myclinic',
      reconnectTimeout: 5000, // ms.
    },
    // Redis.
    session: {
      host: 'localhost',
      port: 6379,
      prefix: 'node_',
    },
  },

  // Session cookie.
  session: {
    key: 'SID',
    secret: 'luke',
  },

  // Log.
  log: {
    prefix: 'api:',
  },
};
